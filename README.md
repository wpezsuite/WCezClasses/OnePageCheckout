## WCezClasses: WooCommerce One Page Checkout

__Streamline your WordPress WooCommerce checkout, and do it The ezWay.__

As inspired by: https://theswedishbear.com/woocommerce-one-page-checkout

   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```
use WPezSuite\WPezClasses\OnePageCheckout\ClassOnePageCheckout as OPC;
use WPezSuite\WPezClasses\OnePageCheckout\ClassHooks as Hooks;

$new_opc = new OPC();
// optioal - if the default does not work...
// set the handle of the stylesheet for the wp_add_inline_style() in the method: wpEnqueueScripts()
$new_opc->setStylesheetHandle( 'TODO');

$new_hooks = new Hooks($new_opc);
$new_hooks->register();
```

### FAQ

__1) Why?__

Another common feature request that's now all set up and ready to go.  

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WCezClasses/WPezAutoload
 
- https://theswedishbear.com/woocommerce-one-page-checkout


### TODO

n/a

### CHANGE LOG

- v.0.0.3 - Monday 20 May 2019
    - CHANGED: Move from WPezClasses to WCezClass, and changed a couple minor bits along the way

- v0.0.2 - Monday 29 April 2019
    - ADDED: wpEnqueueScripts() will only use wp_add_inline_style() if is_checkout() is true 

- v0.0.1 - Monday 29 April 2019
    - Hey! Ho!! Let's go!!!