<?php
// As inspired by: https://theswedishbear.com/woocommerce-change-all-email-titles-in-wc-admin/

namespace WPezSuite\WCezClasses\OnePageCheckout;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'ClassWCOnePageCheckout' ) ) {
	class ClassWCOnePageCheckout implements InterfaceOnePageCheckout {

		protected $_str_redirect_to;
		protected $_bool_remove_cart_border;
		protected $_bool_remove_product_name;
		protected $_bool_remove_product_total;
		protected $_str_stylesheet_handle;


		public function __construct() {

			$this->setPropertyDefaults();
		}

		protected function setPropertyDefaults() {

			$this->_str_redirect_to           = 'shop';
			$this->_bool_remove_cart_border   = true;
			$this->_bool_remove_product_name  = true;
			$this->_bool_remove_product_total = true;
			$this->_str_stylesheet_handle = false;
		}


		public function setRedirectTo( $str = false ) {

			return $this->setString('_str_redirect_to', $str);

		}

		public function setRemoveCartBorder( $bool = true) {

			return $this->setBool('_bool_remove_cart_border', $bool);

		}

		public function setRemoveProductName( $bool = true) {

			return $this->setBool('_bool_remove_product_name', $bool);

		}

		public function setRemoveProductTotal( $bool = true) {

			return $this->setBool('_bool_remove_product_total', $bool);

		}


		public function setStylesheetHandle( $str = false ) {

			return $this->setString('_str_stylesheet_handle', $str);

		}


		public function wpEnqueueScripts() {

			if ( ! function_exists('is_checkout') || is_checkout() === false ) {
				return;

			}

			$str = '';

			if ( $this->_bool_remove_cart_border === true ) {
				$str .= '.woocommerce td, th {border-top: 0 !important; border: none;}';
				$str .= '.woocommerce .woocommerce table.shop_table {border-top: 0 !important; border: none;}';
			}

			if ( $this->_bool_remove_product_name === true ) {
				$str .= '.woocommerce-page.woocommerce-checkout form #order_review th.product-name {display: none !important;}';
				$str .= '.woocommerce-page.woocommerce-checkout form #order_review td.product-name {display: none !important;}';
			}

			if ( $this->_bool_remove_product_total === true ) {

				$str .= '.woocommerce-page.woocommerce-checkout form #order_review th.product-total {display: none !important;}';
				$str .= '.woocommerce-page.woocommerce-checkout form #order_review td.product-total {display: none !important;}';
			}

			if ( $this->_str_stylesheet_handle === false ){
				// https://codex.wordpress.org/Function_Reference/get_stylesheet
				$this->_str_stylesheet_handle = get_stylesheet();
			}

				if ( ! empty( $str ) ) {
					wp_add_inline_style( $this->_str_stylesheet_handle, $str );
				}
		}

		// creates a combined cart and checkout page
		public function woocommerceBeforeCheckoutForm() {

			if ( is_wc_endpoint_url( 'order-received' ) ) {
				return;
			}

			echo '<div class="one-page-checkout-cart">';
			echo do_shortcode( '[woocommerce_cart]' );
			echo '</div' > '';
		}

		// on empty cart when on checkout, redirect to home page
		public function templateRedirect() {

			// TODO - do we need to check for all?
			if ( ! function_exists('is_cart') ||
			     ! function_exists('is_checkout') ||
			     ! function_exists('WC') ||
			     ! function_exists('is_wc_endpoint_url')
			){
				return;
			}

			if ( is_cart() && is_checkout() && WC()->cart->get_cart_contents_count() == 0 && ! is_wc_endpoint_url( 'order-pay' ) && ! is_wc_endpoint_url( 'order-received' ) ) {
				wp_safe_redirect( $this->_str_redirect_to );
				exit;
			}
		}


		// removes coupon field form checkout, do not use CSS for this
		public function removeWoocommerceBeforeCheckoutForm() {

			remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
		}



		protected function setBool( $str_prop = false, $bool = '', $arr_flags = [] ) {

			if ( property_exists( $this, $str_prop ) ) {

				// http://php.net/manual/en/filter.filters.validate.php
				if ( is_array( $arr_flags ) && filter_var( $bool, FILTER_VALIDATE_BOOLEAN, $arr_flags ) ) {

					$this->$str_prop = $bool;

					return true;

				} else {
					$this->$str_prop = (boolean)$bool;

					return true;
				}

				return false;

			}
		}

		protected function setString( $str_prop = false, $str = false, $arr_len = [], $bool_ltrim = true ) {

			$arr_len_default = [
				'min_len' => 0,
				'max_len' => false
			];
			if ( is_array( $arr_len ) ) {

				$arr_len_default = array_merge( $arr_len_default, $arr_len );
			}
			if ( empty ( ltrim( $str ) ) ) {

			}

			if ( property_exists( $this, $str_prop ) && is_string( $str ) ) {

				// without this, e.g., 3x spaces would have a strlen = 3
				if ( $bool_ltrim && empty ( ltrim( $str ) ) ) {
					$str = '';
				}

				if ( strlen( $str ) >= absint( $arr_len_default['min_len'] )
				     && ( $arr_len_default['max_len'] === false || strlen( $str ) <= absint( $arr_len_default['max_len'] ) ) ) {

					$this->$str_prop = $str;
					return true;
				}

				return false;
			}
		}


	}
}