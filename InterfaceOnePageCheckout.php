<?php

namespace WPezSuite\WCezClasses\OnePageCheckout;

interface InterfaceOnePageCheckout {

	public function woocommerceBeforeCheckoutForm();

	public function templateRedirect();

	public function removeWoocommerceBeforeCheckoutForm();

	public function wpEnqueueScripts();

}